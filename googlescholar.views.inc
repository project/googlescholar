<?php

/**
 * @file
 * Views for Google Scholar.
 */

/**
 * Implements hook_views_api().
 */
function googlescholar_views_api() {
  return array('api' => 3.0);
}

/**
 * Implements hook_views_default_views().
 */
function googlescholar_views_default_views() {


  $view = new view();
  $view->name = 'htmlabstracts';
  $view->description = 'Displays a list of HTML abstracts that has been enriched with metadata for Google Scholar.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Google Scholar abstracts';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'List of articles for Google Scholar';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'googlescholar_authors' => 'googlescholar_authors',
    'googlescholar_pubyear' => 'googlescholar_pubyear',
    'title' => 'title',
    'googlescholar_htmlink' => 'googlescholar_htmlink',
    'googlescholar_attachment' => 'googlescholar_attachment',
  );
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = 'Below are the links to published articles. Click on the first link (the title) to access article\'s node on this site. If there is an uploaded article, you may click on the second link to access the HTML abstract and the third link to access the full text of the article.';
  $handler->display->display_options['header']['area']['format'] = 'filtered_html';
  /* Relationship: Content: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'node';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Field: Content: List of authors */
  $handler->display->display_options['fields']['googlescholar_authors']['id'] = 'googlescholar_authors';
  $handler->display->display_options['fields']['googlescholar_authors']['table'] = 'field_data_googlescholar_authors';
  $handler->display->display_options['fields']['googlescholar_authors']['field'] = 'googlescholar_authors';
  $handler->display->display_options['fields']['googlescholar_authors']['label'] = '';
  $handler->display->display_options['fields']['googlescholar_authors']['element_label_colon'] = FALSE;
  /* Field: Content: Year of publication */
  $handler->display->display_options['fields']['googlescholar_pubyear']['id'] = 'googlescholar_pubyear';
  $handler->display->display_options['fields']['googlescholar_pubyear']['table'] = 'field_data_googlescholar_pubyear';
  $handler->display->display_options['fields']['googlescholar_pubyear']['field'] = 'googlescholar_pubyear';
  $handler->display->display_options['fields']['googlescholar_pubyear']['label'] = '';
  $handler->display->display_options['fields']['googlescholar_pubyear']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['googlescholar_pubyear']['alter']['text'] = '([googlescholar_pubyear]).';
  $handler->display->display_options['fields']['googlescholar_pubyear']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Link to HTML abstract */
  $handler->display->display_options['fields']['googlescholar_htmlink']['id'] = 'googlescholar_htmlink';
  $handler->display->display_options['fields']['googlescholar_htmlink']['table'] = 'field_data_googlescholar_htmlink';
  $handler->display->display_options['fields']['googlescholar_htmlink']['field'] = 'googlescholar_htmlink';
  $handler->display->display_options['fields']['googlescholar_htmlink']['label'] = '-';
  $handler->display->display_options['fields']['googlescholar_htmlink']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['googlescholar_htmlink']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['googlescholar_htmlink']['click_sort_column'] = 'url';
  /* Field: Content: Full length version */
  $handler->display->display_options['fields']['googlescholar_attachment']['id'] = 'googlescholar_attachment';
  $handler->display->display_options['fields']['googlescholar_attachment']['table'] = 'field_data_googlescholar_attachment';
  $handler->display->display_options['fields']['googlescholar_attachment']['field'] = 'googlescholar_attachment';
  $handler->display->display_options['fields']['googlescholar_attachment']['label'] = '-';
  $handler->display->display_options['fields']['googlescholar_attachment']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['googlescholar_attachment']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['googlescholar_attachment']['click_sort_column'] = 'fid';
  /* Sort criterion: Content: Year of publication (googlescholar_pubyear) */
  $handler->display->display_options['sorts']['googlescholar_pubyear_value']['id'] = 'googlescholar_pubyear_value';
  $handler->display->display_options['sorts']['googlescholar_pubyear_value']['table'] = 'field_data_googlescholar_pubyear';
  $handler->display->display_options['sorts']['googlescholar_pubyear_value']['field'] = 'googlescholar_pubyear_value';
  $handler->display->display_options['sorts']['googlescholar_pubyear_value']['order'] = 'DESC';
  /* Sort criterion: Content: List of authors (googlescholar_authors) */
  $handler->display->display_options['sorts']['googlescholar_authors_value']['id'] = 'googlescholar_authors_value';
  $handler->display->display_options['sorts']['googlescholar_authors_value']['table'] = 'field_data_googlescholar_authors';
  $handler->display->display_options['sorts']['googlescholar_authors_value']['field'] = 'googlescholar_authors_value';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Contextual filter: User: Name */
  $handler->display->display_options['arguments']['name']['id'] = 'name';
  $handler->display->display_options['arguments']['name']['table'] = 'users';
  $handler->display->display_options['arguments']['name']['field'] = 'name';
  $handler->display->display_options['arguments']['name']['relationship'] = 'uid';
  $handler->display->display_options['arguments']['name']['title_enable'] = TRUE;
  $handler->display->display_options['arguments']['name']['title'] = 'Articles by %1';
  $handler->display->display_options['arguments']['name']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['name']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['name']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['name']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['name']['limit'] = '0';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'htmlabstract' => 'htmlabstract',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  
  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'googlescholar';
  
  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['block_description'] = 'Google Scholar abstracts';
  
  // repeat for each view
  $views[$view->name] = $view;

  $view = new view();
  $view->name = 'yearlyarchive';
  $view->description = 'Displays a list of years that link to Google Scholar abstracts for that year';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Google Scholar year index';
  $view->core = 0;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Yearly archive';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'googlescholar_authors' => 'googlescholar_authors',
    'googlescholar_pubyear' => 'googlescholar_pubyear',
    'title' => 'title',
    'googlescholar_htmlink' => 'googlescholar_htmlink',
    'googlescholar_attachment' => 'googlescholar_attachment',
  );
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Field: Content: List of authors */
  $handler->display->display_options['fields']['googlescholar_authors']['id'] = 'googlescholar_authors';
  $handler->display->display_options['fields']['googlescholar_authors']['table'] = 'field_data_googlescholar_authors';
  $handler->display->display_options['fields']['googlescholar_authors']['field'] = 'googlescholar_authors';
  $handler->display->display_options['fields']['googlescholar_authors']['label'] = '';
  $handler->display->display_options['fields']['googlescholar_authors']['element_label_colon'] = FALSE;
  /* Field: Content: Year of publication */
  $handler->display->display_options['fields']['googlescholar_pubyear']['id'] = 'googlescholar_pubyear';
  $handler->display->display_options['fields']['googlescholar_pubyear']['table'] = 'field_data_googlescholar_pubyear';
  $handler->display->display_options['fields']['googlescholar_pubyear']['field'] = 'googlescholar_pubyear';
  $handler->display->display_options['fields']['googlescholar_pubyear']['label'] = '';
  $handler->display->display_options['fields']['googlescholar_pubyear']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['googlescholar_pubyear']['alter']['text'] = '([googlescholar_pubyear]).';
  $handler->display->display_options['fields']['googlescholar_pubyear']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Link to HTML abstract */
  $handler->display->display_options['fields']['googlescholar_htmlink']['id'] = 'googlescholar_htmlink';
  $handler->display->display_options['fields']['googlescholar_htmlink']['table'] = 'field_data_googlescholar_htmlink';
  $handler->display->display_options['fields']['googlescholar_htmlink']['field'] = 'googlescholar_htmlink';
  $handler->display->display_options['fields']['googlescholar_htmlink']['label'] = '-';
  $handler->display->display_options['fields']['googlescholar_htmlink']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['googlescholar_htmlink']['click_sort_column'] = 'url';
  /* Field: Content: Full length version */
  $handler->display->display_options['fields']['googlescholar_attachment']['id'] = 'googlescholar_attachment';
  $handler->display->display_options['fields']['googlescholar_attachment']['table'] = 'field_data_googlescholar_attachment';
  $handler->display->display_options['fields']['googlescholar_attachment']['field'] = 'googlescholar_attachment';
  $handler->display->display_options['fields']['googlescholar_attachment']['label'] = '-';
  $handler->display->display_options['fields']['googlescholar_attachment']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['googlescholar_attachment']['click_sort_column'] = 'fid';
  /* Sort criterion: Content: List of authors (googlescholar_authors) */
  $handler->display->display_options['sorts']['googlescholar_authors_value']['id'] = 'googlescholar_authors_value';
  $handler->display->display_options['sorts']['googlescholar_authors_value']['table'] = 'field_data_googlescholar_authors';
  $handler->display->display_options['sorts']['googlescholar_authors_value']['field'] = 'googlescholar_authors_value';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Contextual filter: Content: Year of publication (googlescholar_pubyear) */
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['id'] = 'googlescholar_pubyear_value';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['table'] = 'field_data_googlescholar_pubyear';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['field'] = 'googlescholar_pubyear_value';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['default_action'] = 'summary';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['title_enable'] = TRUE;
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['title'] = 'Articles from %1';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['limit'] = '0';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'htmlabstract' => 'htmlabstract',
  );
  
  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'googlescholar-years';
  
  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Year of publication (googlescholar_pubyear) */
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['id'] = 'googlescholar_pubyear_value';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['table'] = 'field_data_googlescholar_pubyear';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['field'] = 'googlescholar_pubyear_value';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['default_action'] = 'summary';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['googlescholar_pubyear_value']['limit'] = '0';
  $handler->display->display_options['block_description'] = 'Google Scholar year index';
  
  // repeat for each view
  $views[$view->name] = $view;

  return $views;
}
