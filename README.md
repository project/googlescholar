## CONTENTS OF THIS FILE

* Introduction
* Recommended modules
* Installation
* Usage
* Fields
* Maintainer


## INTRODUCTION

**Google Scolar Abstract** implements a node content type named “HTML
abstract”.  This content type is designed to capture all metadata
about a research paper that is required by Google Scholar according to
its [Inclusion Guidelines for Webmasters][1].

Google Scholar expects metadata to link to a full length PDF of the
paper.  This module lets the user upload the full length PDF to be
linked.

* For a full description of the module, visit the [project page][2].

* To submit bug reports and feature suggestions, or to track changes
  use the project's [issue queue][3].


## REQUIREMENTS

* [**Advanced help hint**][4]:<br>
  To hint about how to get a nice display of README.md.

* [**Link**][5]:<br>
  To have a <em>link</em> widget for the <em>HTML abstract</em>
  content type.

## RECOMMENDED MODULES

* [**Views**][6]:<br>
  When this module is enabled, there will be two views enabled to give
  easy access to the nodes belonging to the *HTML abstract* type.

  The view named *Google Scholar abstracts* will display a list
  pointing to published content of this type.  The path
  `/googlescholar` will list all content. The path
  `/googlescholar/USERNAME` (where `USERNAME` is the username who owns
  this node) will list the content created by this user.  For a block
  view, enable the block named *Google Scholar abstracts*.

  The view named *Google Scholar year index* displays a list of years
  with linked content, and lists of published content belonging to a
  particular year.  The paths are `/googlescholar-years` and the
  `/googlescholar-years/YYYY` (where `YYYY` are the year).  For a
  block view, enable *Google Scholar year index*.

  (Please note that Views has [**ctools**][7] as a dependency.)

* [**Markdown filter**][8]:<br>
  When this module is enabled, display of the project's `README.md` will be
  rendered with markdown when you visit `/admin/help/googlescholar`.


## INSTALLATION

Install as you would normally install a contributed Drupal
module. See: [Installing modules][9] for further information.

Navigate to *Administration → People → Permissions*, scroll down to
the *Node* section and set the permissions for the roles you want to
allow for the *HTML abstract* content type.

There are no further adminstrative settings, the *HTML abstract*
content type will appear when you enable the module, and disappear
when you disable it.

Note that uninstalling the module will also purge *all* content of
this content type. This operation is irreversible.


## USAGE

To create an HTML abstract for *Google Scholar*, use the *Add content*
link as you would with any other node type.  Select the content type
*HTML abstract* and fill in the fields as indicated by the
instructions in the form.

In addition to the uploaded full length version of the paper (saved in
the same directory as the HTML abstract), you may also link to a free
off-site version and the version of record.

The metadata for *Google Scholar* will be extracted from the *Citation
suggestion* field.  For good metadata, you need to stay as close as
possible to this format:

    Principal author (Year). "Full title of paper." Name of journal/conference.  Volume:Issue pp. firstpage-lastpage

For a &#8220;corporate author&#8221;, put a comma after the name of
corporation or project.  Example:

    Our Project, (2007). "FaceRank. An Algorithm for Evaluating Facebook Friendliness". Journal of Irreproducible Research

If the metadata is not parsed correctly, first make sure that you've
put quote marks around the title and that there are full stops
between the main parts.

Tick the box *Conference proceeding* if the paper is from a conference
proceeding and not from a journal.

While it is not required, it is recommended that you fill in the
*Abstract* field. You should just copy this from the PDF.

Use the field *Co-authors* to add co-authors. Type in one author per
field, and press “Add another item” to save and add an empty field.

The *DOI* field lets you enter an unique Digital Object Identifier for
the document.

When done, press *Save* to save the uploaded file and the metadata.

## FIELDS

The following fields make up the *HTML abstract* bundle:

* `title` : Full document title
* `body` : Citation suggestion
* `googlescholar_abstract` : Full length version
* `googlescholar_attachment` : File attachment
* `googlescholar_weblink` : Link to free off-site version
* `googlescholar_vorlink` : Link to version of record
* `googlescholar_pubtype` : Type of publication
* `googlescholar_coauthors` : Co-authors
* `googlescholar_doi` : Digital Object Identifier
* `googlescholar_primfirst` : Primary author first name(s)
* `googlescholar_primlast` : Primary author last name
* `googlescholar_pubyear` : Year of publication
* `googlescholar_journal` : Journal/conference
* `googlescholar_volumeissue` : Volume and issue
* `googlescholar_pagespan` : Pages
* `googlescholar_license` : Link to license (not used)
* `googlescholar_htmlink` : Link to HTML abstract

Note that some of the fields, starting with “Primary author”, are
hidden, but exists to simply integration with **Views**.


## MAINTAINER

* [gisle](https://www.drupal.org/u/gisle) (current maintainer)

Any help with development (patches, reviews, comments) are welcome.


[1]: http://scholar.google.no/intl/en/scholar/inclusion.html#indexing
[2]: https://www.drupal.org/project/googlescholar
[3]: https://www.drupal.org/project/issues/googlescholar
[4]: https://www.drupal.org/project/advanced_help_hint
[5]: https://www.drupal.org/project/link
[6]: https://www.drupal.org/project/views
[7]: https://www.drupal.org/project/ctools
[8]: https://www.drupal.org/project/markdown
[9]: https://www.drupal.org/documentation/install/modules-themes/modules-7
